# CIAI 16/17 #


### Lab 1 ###

Please find here the lab assignment. [version 1](https://bitbucket.org/jacome/ciai1617/raw/424b03b40f91d35255176118fcae897dda58424e/docs/lab1.pdf) [version 2](https://bitbucket.org/jacome/ciai1617/raw/bbcba2f21d0980901a6075ef6535c16526b9b671/docs/lab1.pdf)

### Lab 2 ###

Please find here the lab assignment. [version 1](https://bitbucket.org/jacome/ciai1617/raw/063cc54d621dae32d1a5c8a8fa1aa1e1a487badc/docs/lab2.pdf)

### Lab 3 ###

Please find here the lab assignment. [version 1](https://bitbucket.org/jacome/ciai1617/raw/5b01ed7189e805c1046b738a73407ce7c5826d05/docs/lab3.pdf)
